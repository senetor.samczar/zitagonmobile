import { Component } from '@angular/core'
import { StatusBar } from '@ionic-native/status-bar'
import { SplashScreen } from '@ionic-native/splash-screen'
import { Platform } from 'ionic-angular'
import { Storage } from '@ionic/storage'
import { TranslateService } from '@ngx-translate/core'
import { BackgroundMode } from '@ionic-native/background-mode'
import { Diagnostic } from '@ionic-native/diagnostic'
import { ImageLoaderConfig } from 'ionic-image-loader'

import { TabsPage } from '../pages/tabs/tabs'
import { Login } from '../pages/login/login'
import { AuthService } from '../providers/auth-service'

import * as firebase from 'firebase'
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any
  public isLoggedIn: boolean

  constructor(
    public storage: Storage,
    public bg: BackgroundMode,
    public translate: TranslateService,
    public afService: AuthService,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public diagnostic: Diagnostic,
    public imageLoaderConfig: ImageLoaderConfig
  ) {
    this.platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        this.diagnostic
          .requestExternalStorageAuthorization()
          .then(() => {})
          .catch(error => {})
      }

      translate.setDefaultLang('en')

      storage.get('lang').then(val => {
        if (val != null) {
          translate.setDefaultLang(val)
        } else {
          translate.setDefaultLang('en')
        }
      })

      this.afService.afAuth.authState.subscribe((user: firebase.User) => {
        if (!user) {
          this.isLoggedIn = false
          this.rootPage = Login
        } else {
          this.isLoggedIn = true
          this.rootPage = TabsPage
        }
      })

      // enable debug mode to get console logs and stuff
      this.imageLoaderConfig.enableDebugMode()
      // set a fallback url to use by default in case an image is not found
      this.imageLoaderConfig.setFallbackUrl('assets/imgs/icon.png')
      this.imageLoaderConfig.enableFallbackAsPlaceholder(true)
      this.imageLoaderConfig.setImageReturnType('base64')
      this.imageLoaderConfig.setSpinnerColor('secondary')
      this.imageLoaderConfig.setSpinnerName('bubbles')
      this.imageLoaderConfig.setCacheDirectoryName(
        'zitagon-cache-directory-for-images'
      )

      this.imageLoaderConfig.setMaximumCacheSize(20 * 1024 * 1024) // set max size to 20MB
      this.imageLoaderConfig.setMaximumCacheAge(7 * 24 * 60 * 60 * 1000) // 7 days

      firebase
        .messaging()
        .getToken()
        .then(t => {})
        .catch(e => {})
      firebase.messaging().onMessage(x => {})

      this.statusBar.overlaysWebView(true)
      this.statusBar.styleBlackTranslucent()
      this.statusBar.backgroundColorByHexString('#734b6d')

      this.splashScreen.hide()
    })
  }
}
