import { NgModule, ErrorHandler } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular'
import { MyApp } from './app.component'
import { Facebook } from '@ionic-native/facebook'
import { GooglePlus } from '@ionic-native/google-plus'
import { InAppBrowser } from '@ionic-native/in-app-browser'
import { StatusBar } from '@ionic-native/status-bar'
import { SplashScreen } from '@ionic-native/splash-screen'
import { SocialSharing } from '@ionic-native/social-sharing'
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
import { BackgroundMode } from '@ionic-native/background-mode'
import { Keyboard } from '@ionic-native/keyboard'
import { IonicStorageModule } from '@ionic/storage'
import { ScreenOrientation } from '@ionic-native/screen-orientation'
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer'
import { FilePath } from '@ionic-native/file-path'
import { HttpClientModule, HttpClient } from '@angular/common/http'
import { Diagnostic } from '@ionic-native/diagnostic'
import { IonicImageLoader } from 'ionic-image-loader'
import { IonMarqueeModule } from 'ionic-marquee'
import { File } from '@ionic-native/file'
import { Network } from '@ionic-native/network'
import { AngularFireModule } from 'angularfire2'
import { AngularFireDatabaseModule } from 'angularfire2/database'
import { AngularFireAuthModule } from 'angularfire2/auth'

import { Player } from '../pages/player/player'
import { Video } from '../pages/video/video'
import { Safe } from '../pipes/safe'
import { Share } from '../pages/share/share'
import { About } from '../pages/about/about'
import { Menu } from '../pages/menu/menu'
import { SearchPage } from '../pages/search/search'
import { LibraryPage } from '../pages/library/library'
import { PlaylistPage } from '../pages/playlist/playlist'
import { FavoritesPage } from '../pages/favorites/favorites'
import { HomePage } from '../pages/home/home'
import { MoodsPage } from '../pages/moods/moods'
import { Mood } from '../pages/mood/mood'
import { Artist } from '../pages/artist/artist'
import { Album } from '../pages/album/album'
import { LyricsPage } from '../pages/lyrics/lyrics'
import { PlistPage } from '../pages/plist/plist'
import { FollowedArtistsPage } from '../pages/followed-artists/followed-artists'
import { CountrylistPage } from '../pages/countrylist/countrylist'
import { TabsPage } from '../pages/tabs/tabs'
import { SharedModule } from './shared/shared.module'
import { IonicAudioModule } from './shared/audio/ionic-audio.module'
import { AuthService } from '../providers/auth-service'
import { Login } from '../pages/login/login'
import { ExplorePage } from '../pages/explore/explore'
import { CountryPage } from '../pages/country/country'
import { RegionPage } from '../pages/region/region'
import { WelcomePage } from '../pages/welcome/welcome'
import { Offline } from '../providers/offline'

//Replace this with your own keys from console.firebase.com
export const firebaseConfig = {
  apiKey: 'AIzaSyBC1ka7R_E19cuXjZDzKv46Y-KIzFGAKKg',
  authDomain: 'zitagon-9155a.firebaseapp.com',
  databaseURL: 'https://zitagon-9155a.firebaseio.com',
  projectId: 'zitagon-9155a',
  storageBucket: 'zitagon-9155a.appspot.com',
  messagingSenderId: '1064318733936'
  // apiKey: "AIzaSyCS6_oV9mHIqhGAn2Xn6icfaNrzzJ-7fAY",
  // authDomain: "zitagon-staging.firebaseapp.com",
  // databaseURL: "https://zitagon-staging.firebaseio.com",
  // projectId: "zitagon-staging",
  // storageBucket: "zitagon-staging.appspot.com",
  // messagingSenderId: "838875225403"
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json')
}

@NgModule({
  declarations: [
    Safe,
    MyApp,
    WelcomePage,
    SearchPage,
    Player,
    About,
    LibraryPage,
    FavoritesPage,
    PlaylistPage,
    MoodsPage,
    Mood,
    ExplorePage,
    RegionPage,
    CountryPage,
    CountrylistPage,
    HomePage,
    TabsPage,
    Artist,
    Album,
    Login,
    Share,
    Menu,
    Video,
    LyricsPage,
    FollowedArtistsPage,
    PlistPage
  ],
  imports: [
    HttpClientModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    SharedModule.forRoot(),
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: ''
    }),
    IonicAudioModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicImageLoader.forRoot(),
    IonMarqueeModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    WelcomePage,
    SearchPage,
    LibraryPage,
    FavoritesPage,
    PlaylistPage,
    HomePage,
    MoodsPage,
    Mood,
    About,
    Player,
    Artist,
    Album,
    CountryPage,
    CountrylistPage,
    ExplorePage,
    RegionPage,
    TabsPage,
    Login,
    Share,
    Menu,
    LyricsPage,
    Video,
    FollowedArtistsPage,
    PlistPage
  ],
  providers: [
    FilePath,
    FileTransfer,
    File,
    FileTransferObject,
    ScreenOrientation,
    BackgroundMode,
    InAppBrowser,
    AuthService,
    Keyboard,
    Facebook,
    GooglePlus,
    StatusBar,
    SplashScreen,
    SocialSharing,
    Diagnostic,
    Network,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Offline
  ]
})
export class AppModule {}
