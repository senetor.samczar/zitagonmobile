import { Injectable } from '@angular/core'
import { Platform, AlertController } from 'ionic-angular'
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer'
import { File } from '@ionic-native/file'
import { Storage } from '@ionic/storage'
import { FilePath } from '@ionic-native/file-path'
declare var cordova: any

/*
  Generated class for the OfflineProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Offline {
  storageDirectory: string = ''

  constructor(
    public filePath: FilePath,
    public alertCtrl: AlertController,
    public transfer: FileTransfer,
    public file: File,
    public storage: Storage,
    public platform: Platform
  ) {
    this.platform.ready().then(() => {
      // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
      if (!this.platform.is('cordova')) {
        return false
      }

      if (this.platform.is('ios')) {
        this.storageDirectory = file.documentsDirectory
      } else if (this.platform.is('android')) {
        this.storageDirectory = file.externalRootDirectory + 'firespotify/'
      } else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false
      }
      localStorage.setItem('downloadList', JSON.stringify([]))
    })
  }

  download(file, name, isFull = false) {
    let obj = file
    if (isFull) {
      file = obj.src
    }

    this.platform.ready().then(() => {
      const fileTransfer: FileTransferObject = this.transfer.create()
      const imageLocation = file //encodeURI(file);
      let dir = cordova.file.dataDirectory

      fileTransfer.download(imageLocation, dir + name, true).then(
        entry => {
          if (isFull) {
            obj.src = entry.toURL()
            name = name.replace('.mpg3', '.png')
            obj.art = dir + name
            obj.name = name
            let dList = JSON.parse(localStorage.getItem('downloadList'))
            dList.push(obj)
            localStorage.setItem('downloadList', JSON.stringify(dList))
          }
        },
        error => {}
      )
    })
  }

  retrieve(name): any {
    this.file
      .checkFile(this.storageDirectory, name)
      .then(x => {
        this.file.readAsDataURL
        const path = this.storageDirectory + name.replace(/ /g, '%20')

        return path.toString()
      })
      .catch(err => {
        return false
      })
  }
}
