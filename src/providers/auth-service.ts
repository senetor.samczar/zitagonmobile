import { Injectable, Inject } from '@angular/core'
import { AngularFireDatabase } from 'angularfire2/database'
import { AngularFireAuth } from 'angularfire2/auth'
import { FirebaseApp } from 'angularfire2'
import * as firebase from 'firebase'

import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/first'
import { Facebook } from '@ionic-native/facebook'
import { Platform, AlertController, LoadingController } from 'ionic-angular'

@Injectable()
export class AuthService {
  public authState: AngularFireAuth
  uid: any
  loading: any

  constructor(
    public alertCtrl: AlertController,
    public af: AngularFireDatabase,
    public afAuth: AngularFireAuth,
    @Inject(FirebaseApp) public fire: any,
    public platform: Platform,
    public fb: Facebook,
    public loadingCtrl: LoadingController
  ) {
    afAuth.authState.subscribe((user: firebase.User) => {
      if (!user) {
        this.uid = null
        return
      }
      this.uid = user.uid
    })
  }

  public checkLoading() {
    let maxLoadingTime = 10000
    if (this.loading) {
      setTimeout(() => {
        this.hideLoading()
      }, maxLoadingTime)
    }
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: ''
    })
    this.loading.present()
    this.checkLoading()
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss()
      this.loading = null
    }
  }

  signInWithFacebook() {
    this.afAuth.auth
      .signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(auth => {
        let fbRes: any = auth
        let fbcredential = firebase.auth.FacebookAuthProvider.credential(
          fbRes.credential.accessToken
        )
        this.showLoading()
        firebase
          .auth()
          .signInWithCredential(fbcredential)
          .then(success => {
            fbRes.additionalUserInfo.profile
            firebase.auth().currentUser.uid
            this.hideLoading()
          })
          .catch(error => {
            this.hideLoading()
            alert(error['code'])
          })
      })
      .catch(err => {
        alert('error: ' + JSON.stringify(err))
      })
  }

  signInWithGoogle() {
    this.afAuth.auth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(auth => {
        let gpRes: any = auth
        let credential = firebase.auth.GoogleAuthProvider.credential(
          gpRes.credential.idToken,
          gpRes.credential.accessToken
        )
        this.showLoading()
        firebase
          .auth()
          .signInWithCredential(credential)
          .then(success => {
            firebase.auth().currentUser.uid
            gpRes.additionalUserInfo.profile
            this.hideLoading()
          })
          .catch(error => {
            this.hideLoading()
            alert(error['code'])
          })
      })
      .catch(err => {
        alert('error: ' + JSON.stringify(err))
      })
  }

  registerUser(email, password, password2): firebase.Promise<AngularFireAuth> {
    return this.afAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .then((res: AngularFireAuth) => {
        this.af.object('/users/' + this.uid).subscribe(res => {
          if (res.role > -1) {
          } else {
            this.af.object('/users/' + this.uid + '/role').set(0)
          }
        })

        return res
      })
      .catch(error => {
        throw error
      })
  }

  signIn(email, password): firebase.Promise<AngularFireAuth> {
    return this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(res => {
        this.af.object('/users/' + this.uid).subscribe(res => {
          if (res.role > -1) {
          } else {
            this.af.object('/users/' + this.uid + '/role').set(0)
          }
        })
      })
      .catch(error => {
        throw error
      })
  }

  signOut(): void {
    firebase.auth().signOut()
  }

  forgotPassword(email): firebase.Promise<AngularFireAuth> {
    return firebase
      .app()
      .auth()
      .sendPasswordResetEmail(email)
      .then(s => {
        let alert = this.alertCtrl.create({
          title: 'Password Reset',
          subTitle: 'Check your inbox to reset your password',
          buttons: ['Dismiss']
        })
        alert.present()
      })
      .catch(error => {
        throw error
      })
  }
}
