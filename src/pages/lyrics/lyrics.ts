import { Component } from '@angular/core'
import { NavController, NavParams } from 'ionic-angular'
import { MusicService } from '../../providers/musicservice'
import { MusicData } from '../../providers/music-data'

@Component({
  selector: 'page-lyrics',
  templateUrl: 'lyrics.html'
})
export class LyricsPage {
  public track

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public md: MusicData,
    public musicservice: MusicService
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LyricsPage')
    this.track = this.navParams.get('track')
    console.log(this.track)
  }

  back() {
    this.navCtrl.pop()
  }
}
