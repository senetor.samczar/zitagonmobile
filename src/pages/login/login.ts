import { Component } from '@angular/core'
import * as firebase from 'firebase'
import { Storage } from '@ionic/storage'
import { Keyboard } from '@ionic-native/keyboard'
import { Facebook } from '@ionic-native/facebook'
import { GooglePlus } from '@ionic-native/google-plus'
import {
  NavController,
  NavParams,
  Platform,
  AlertController
} from 'ionic-angular'
import { SplashScreen } from '@ionic-native/splash-screen'
// import { AngularFireDatabase } from 'angularfire2/database'
import { BackgroundMode } from '@ionic-native/background-mode'

import { TabsPage } from '../tabs/tabs'
import { AuthService } from '../../providers/auth-service'

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class Login {
  loginData: any
  registerData: any
  registerPage: boolean = false
  loginPage: boolean = false
  fb: boolean = false

  constructor(
    public alertCtrl: AlertController,
    public kb: Keyboard,
    public bg: BackgroundMode,
    public storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    //af: AngularFireDatabase,
    private afService: AuthService,
    platform: Platform,
    splashScreen: SplashScreen,
    public facebook: Facebook,
    public google: GooglePlus
  ) {
    this.kb.onKeyboardShow().subscribe(data => {
      this.fb = true
    })

    this.kb.onKeyboardHide().subscribe(data => {
      this.fb = false
    })

    platform.ready().then(() => {
      platform.registerBackButtonAction(() => {
        if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop()
        } else {
          this.bg.moveToBackground()
        }
      })

      if (splashScreen) {
        setTimeout(() => {
          splashScreen.hide()
        }, 500)
      }
    })

    this.afService.afAuth.authState.subscribe((user: firebase.User) => {
      if (!user) {
      } else {
        this.navCtrl.setRoot(TabsPage)
      }
    })

    this.loginData = { email: '', password: '' }
    this.registerData = { email: '', password: '', password2: '' }

    storage.get('email').then(val => {
      this.loginData.email = val
    })
    storage.get('password').then(val => {
      this.loginData.password = val
    })
  }

  page() {
    this.registerPage = !this.registerPage
  }

  goSignUp() {
    this.loginPage = false
    this.registerPage = true
  }

  goSignIn() {
    this.loginPage = true
    this.registerPage = false
  }

  back() {
    this.loginPage = false
    this.registerPage = false
  }

  signInWithFacebook(): void {
    const provider = new firebase.auth.FacebookAuthProvider()
    firebase
      .auth()
      .signInWithPopup(provider)
      .then(response => {
        let token = response.credential.accessToken
        let credential = firebase.auth.FacebookAuthProvider.credential(token)
        firebase
          .auth()
          .signInWithCredential(credential)
          .then(success => {})
          .catch(err => {
            throw err
          })
      })
      .catch(function(error) {})
  }

  signInWithGoogle(): void {
    console.log('googleLogin')
    const provider = new firebase.auth.GoogleAuthProvider()
    firebase
      .auth()
      .signInWithRedirect(provider)
      .then(response => {
        let token = response.credential.accessToken
        let credential = firebase.auth.GoogleAuthProvider.credential(
          token,
          null
        )
        firebase
          .auth()
          .signInWithCredential(credential)
          .then(success => {})
          .catch(err => {})
      })
      .catch(function(error) {})

    // let googleClientId = "1064318733936-v69s85erennjr33nl3bban27vq5l4i6h.apps.googleusercontent.com";
    // this.google.login({ 'webClientId': googleClientId }).then((res) => {
    //   this.afService.showLoading();
    //   let credential = firebase.auth.GoogleAuthProvider.credential(res.idToken);
    //   firebase.auth().signInWithCredential(credential).then((success) => {
    //     this.afService.hideLoading();
    //   }).catch((err) => {
    //     this.afService.hideLoading();
    //     let code = err['code'];
    //     alert(code);
    //   })
    // }).catch(e => alert("Error logging into Google: " + JSON.stringify(e)));
  }

  signIn(): void {
    if (
      this.loginData.email != undefined ||
      this.loginData.password != undefined
    ) {
      this.afService
        .signIn(this.loginData.email, this.loginData.password)
        .then(x => {
          this.storage.set('email', this.loginData.email)
          this.storage.set('password', this.loginData.password)
        })
        .catch(error => {
          let alert = this.alertCtrl.create({
            title: 'Login',
            subTitle: 'Email Address or Password Incorrect',
            buttons: ['Dismiss']
          })
          alert.present()
        })
    } else if (this.loginData.email == undefined) {
      let alert = this.alertCtrl.create({
        title: 'Login',
        subTitle: 'Email Address or Password Can not Be Empty',
        buttons: ['Dismiss']
      })
      alert.present()
    } else if (this.loginData.password == undefined) {
      let alert = this.alertCtrl.create({
        title: 'Login',
        subTitle: 'Email Address or Password Can not Be Empty',
        buttons: ['Dismiss']
      })
      alert.present()
    } else {
    }
  }
  register(): void {
    this.afService
      .registerUser(
        this.registerData.email,
        this.registerData.password,
        this.registerData.password2
      )
      .then(x => {
        this.storage.set('email', this.registerData.email)
        this.storage.set('password', this.registerData.password)
      })
      .catch(error => {
        let alert = this.alertCtrl.create({
          title: 'Login',
          subTitle: 'Email Address is not valid or already in use',
          buttons: ['Dismiss']
        })
        alert.present()
      })
  }

  forgotPassword(): void {
    this.afService.forgotPassword(this.loginData.email).catch(error => {
      let alert = this.alertCtrl.create({
        title: 'Password Reset',
        subTitle: error.message,
        buttons: ['Dismiss']
      })
      alert.present()
    })
  }

  // private onSignInSuccess(): void {
  //   this.navCtrl.setRoot(TabsPage)
  // }
}
