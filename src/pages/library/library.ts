import { Component } from '@angular/core'
import {
  NavController,
  Platform,
  AlertController,
  ActionSheetController
} from 'ionic-angular'
import { IntervalObservable } from 'rxjs/observable/IntervalObservable'
// import { File } from '@ionic-native/file'
// import { FilePath } from '@ionic-native/file-path'
import { BackgroundMode } from '@ionic-native/background-mode'
import {
  AngularFireDatabase,
  FirebaseListObservable
} from 'angularfire2/database'

import { Player } from '../player/player'
import { MusicService } from '../../providers/musicservice'
import { Favorite } from '../../providers/favorite'
import { MusicData } from '../../providers/music-data'
import { PlaylistPage } from '../playlist/playlist'
import { FavoritesPage } from '../favorites/favorites'
import { Artist } from '../artist/artist'
import { WelcomePage } from '../welcome/welcome'
import { Menu } from '../menu/menu'
import { PlistPage } from '../plist/plist'

@Component({
  selector: 'page-library',
  templateUrl: 'library.html'
})
export class LibraryPage {
  tracks: any
  playlists: FirebaseListObservable<any>
  artists: any[] = []
  constructor(
    public bg: BackgroundMode,
    public actionSheetCtrl: ActionSheetController,
    public alert: AlertController,
    public platform: Platform,
    public navCtrl: NavController,
    public musicservice: MusicService,
    public fav: Favorite,
    public af: AngularFireDatabase,
    public md: MusicData,
    // private file: File,
    // private filePath: FilePath
  ) {
    platform.ready().then(() => {
      platform.registerBackButtonAction(() => {
        if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop()
        } else {
          this.bg.moveToBackground()
        }
      })
    })
    this.playlists = this.md.playlists
    this.md.playlists.subscribe(x => {})
    this.tracks = this.musicservice.audio.tracks[0]

    this.fav.following.subscribe(fav => {
      this.artists = []
      fav.forEach(element => {
        this.md.artists.subscribe(tracks => {
          tracks.forEach(track => {
            if (element.$key == track.$key) {
              this.artists.push({
                cover: track.cover,
                image: track.image,
                name: track.name,
                key: track.$key
              })
            }
          })
        })
      })
    })
  }

  playmix(track) {
    this.musicservice.play(this.fav.allpopular, track)
    this.tracks = this.musicservice.audio.tracks[0]
  }

  followArtist() {
    this.navCtrl.push(WelcomePage)
  }

  favArtist() {
    this.navCtrl.push(WelcomePage, { fav: true })
  }

  favorites() {
    this.navCtrl.push(FavoritesPage)
  }

  openPlayList() {
    this.navCtrl.push(PlistPage, {
      playlist: null,
      name: name
    })
  }

  playlist(id, name) {
    this.navCtrl.push(PlaylistPage, {
      playlist: id,
      name: name
    })
  }

  gotoartist(artistKey) {
    this.navCtrl.push(Artist, { artistKey: artistKey })
  }

  ngAfterContentInit() {
    // get all tracks managed by AudioProvider so we can control playback via the APIs
    this.tracks = this.musicservice.audio.tracks[0]

    IntervalObservable.create(200).subscribe(n => {
      this.tracks = this.musicservice.audio.tracks[0]
    })
  }

  ionViewWillEnter() {
    this.tracks = this.musicservice.audio.tracks[0]
  }

  next() {
    this.musicservice.next()
    this.tracks = this.musicservice.audio.tracks[0]
  }

  finish() {
    this.musicservice.finish()
  }

  player() {
    this.navCtrl.push(Player)
  }

  menu() {
    this.navCtrl.push(Menu)
  }
}
