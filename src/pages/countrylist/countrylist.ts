import { Component } from '@angular/core'
import {
  Platform,
  NavController,
  ActionSheetController,
  NavParams
} from 'ionic-angular'
import {
  AngularFireDatabase,
  FirebaseListObservable
} from 'angularfire2/database'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'
import { BackgroundMode } from '@ionic-native/background-mode'

import { MusicData } from '../../providers/music-data'
// import { AuthService } from '../../providers/auth-service'
import { Favorite } from '../../providers/favorite'
import { MusicService } from '../../providers/musicservice'
import { CountryPage } from '../country/country'

/**
 * Generated class for the CountrylistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-countrylist',
  templateUrl: 'countrylist.html'
})
export class CountrylistPage {
  tracks: any
  newVideos: any[] = []
  newArtists: FirebaseListObservable<any>
  newAlbums: FirebaseListObservable<any>
  newTracks: FirebaseListObservable<any>
  allTracks: FirebaseListObservable<any>
  allVideos: FirebaseListObservable<any>
  allAlbums: FirebaseListObservable<any>
  allArtists: FirebaseListObservable<any>
  allCountries: FirebaseListObservable<any>
  popularTracks: FirebaseListObservable<any>
  popularVideos: FirebaseListObservable<any>
  popularAlbums: FirebaseListObservable<any>
  popularArtists: FirebaseListObservable<any>
  newPlaylists: FirebaseListObservable<any>
  regions: FirebaseListObservable<any>
  myTracks: any
  playlists: any[] = []
  regionPlaylists: any[] = [] //Region playlist Array holder
  videoPlaylists: any[] = []
  popularTracksPlaylists: any[] = []
  popularVideosPlaylists: any[] = []
  popularArtistsPlaylists: any[] = []
  popularAlbumsPlaylists: any[] = []
  regionsList: any[] = []
  videob: boolean = false
  trackb: boolean = false
  artistb: boolean = false
  albumb: boolean = false
  countries: any[] = []
  countrieslist: FirebaseListObservable<any>
  region: any
  actions: any
  opened: boolean = false
  tabBarElement: any
  curcountryList: any[] = []

  constructor(
    public bg: BackgroundMode,
    public platform: Platform,
    public favorite: Favorite,
    public actionSheetCtrl: ActionSheetController,
    public params: NavParams,
    // private _auth: AuthService,
    public navCtrl: NavController,
    public musicservice: MusicService,
    public af: AngularFireDatabase,
    public md: MusicData
  ) {
    platform.ready().then(() => {
      platform.registerBackButtonAction(() => {
        if (this.opened) {
          this.opened = false
          setTimeout(() => {
            this.actions.dismiss()
          }, 100)
        } else if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop()
        } else {
          this.bg.moveToBackground()
        }
      })
    })

    this.allAlbums = this.md.albums
    this.allArtists = this.md.artists
    this.allTracks = this.md.tracks
    this.allVideos = this.md.videos
    this.regions = this.md.regions
    this.allCountries = this.md.countrys
    this.newArtists = this.md.newArtists
    this.newAlbums = this.md.newAlbums
    this.newTracks = this.md.newTracks
    this.newPlaylists = this.md.newPlaylists
    this.popularTracks = this.md.popularTrack
    this.popularVideos = this.md.popularVideos
    this.popularAlbums = this.md.popularAlbum
    this.popularArtists = this.md.popularArtist

    this.region = this.params.get('region')

    af.list('/countrys/').subscribe(snapshot => {
      this.curcountryList = []
      snapshot.forEach(element => {
        if (
          element.countryRegion == this.region.name ||
          element.countryName == this.region.name ||
          element.regionName == this.region.name
        ) {
          this.curcountryList.push({
            src: element.cover,
            name: element.name,
            key: element.key,
            image: element.image
          })
        }
      })
    })

    this.tabBarElement = document.querySelector('.tabbar.show-tabbar')
    this.tabBarElement.style.display = 'none'
  }

  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex'
  }

  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none'
  }

  gotocountry(country) {
    this.navCtrl.push(CountryPage, { country: country })
  }
}
