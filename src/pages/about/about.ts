import { Component } from '@angular/core'
import {
  NavController,
  NavParams,
  ActionSheetController,
  Platform
} from 'ionic-angular'
import { BackgroundMode } from '@ionic-native/background-mode'

import { MusicService } from '../../providers/musicservice'
// import { AuthService } from '../../providers/auth-service'
import { Favorite } from '../../providers/favorite'

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class About {
  tabBarElement: any
  constructor(
    public bg: BackgroundMode,
    public platform: Platform,
    public favorite: Favorite,
    public actionSheetCtrl: ActionSheetController,
    public params: NavParams,
    // private _auth: AuthService,
    public navCtrl: NavController,
    public musicservice: MusicService
  ) {
    platform.ready().then(() => {
      platform.registerBackButtonAction(() => {
        if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop()
        } else {
          this.bg.moveToBackground()
        }
      })
    })

    this.tabBarElement = document.querySelector('.tabbar.show-tabbar')
    this.tabBarElement.style.display = 'none'
  }
}
