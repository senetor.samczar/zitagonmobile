import { Component } from '@angular/core'
import {
  NavController,
  Platform,
  AlertController,
  ActionSheetController
} from 'ionic-angular'
import { AngularFireDatabase } from 'angularfire2/database'
import { BackgroundMode } from '@ionic-native/background-mode'

import { MusicService } from '../../providers/musicservice'
import { Favorite } from '../../providers/favorite'
import { MusicData } from '../../providers/music-data'
import { Player } from '../player/player'
import { PlaylistPage } from '../playlist/playlist'
@Component({
  selector: 'page-plist',
  templateUrl: 'plist.html'
})
export class PlistPage {
  tracks: any
  plist = []
  actions: any

  constructor(
    public bg: BackgroundMode,
    public actionSheetCtrl: ActionSheetController,
    public alert: AlertController,
    public platform: Platform,
    public navCtrl: NavController,
    public musicservice: MusicService,
    public fav: Favorite,
    public af: AngularFireDatabase,
    public md: MusicData
  ) {
    platform.ready().then(() => {
      platform.registerBackButtonAction(() => {
        if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop()
        } else {
          this.bg.moveToBackground()
        }
      })
    })

    this.md.playlists.subscribe(p => {
      this.plist = []
      let temp = []
      p.forEach(pelem => {
        temp.push(pelem)
        this.af
          .list('playlists/' + this.md.user + '/' + pelem.$key + '/tracks')
          .subscribe(fav => {
            pelem.myTracks = []
            fav.forEach(element => {
              this.md.tracks.subscribe(tracks => {
                tracks.forEach(track => {
                  if (element.$key == track.$key) {
                    pelem.myTracks.push({
                      src: track.url,
                      video: track.video,
                      artist: track.artistName,
                      title: track.name,
                      art: track.albumArt,
                      preload: 'metadata',
                      key: track.$key,
                      artistId: track.artist,
                      albumId: track.album,
                      album: track.albumName,
                      artistName: track.artistName
                    })
                    this.sortList(temp)
                  }
                })
              })
            })
          })
      })
    })
  }

  sortList(ary) {
    this.plist = []
    for (let i = ary.length - 1; i > -1; i--) {
      this.plist.push(ary[i])
    }
  }

  more(track) {
    this.actions = this.actionSheetCtrl.create({
      title: track.name,
      buttons: [
        {
          text: 'Remove',
          handler: () => {
            this.fav.removePlaylist(track.$key)
          }
        }
      ]
    })

    this.actions.present()
  }

  ionViewWillEnter() {
    this.tracks = this.musicservice.audio.tracks[0]
  }

  next() {
    this.musicservice.next()
    this.tracks = this.musicservice.audio.tracks[0]
  }

  player() {
    this.navCtrl.push(Player)
  }

  playlist(id, name) {
    this.navCtrl.push(PlaylistPage, {
      playlist: id,
      name: name
    })
  }

  createPlaylist() {
    this.fav.addNewPlaylistAlert()
  }
}
