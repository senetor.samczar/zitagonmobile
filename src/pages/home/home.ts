import { Component } from '@angular/core'
import {
  NavController,
  Platform,
  AlertController,
  NavParams
} from 'ionic-angular'
import { IntervalObservable } from 'rxjs/observable/IntervalObservable'
import {
  AngularFireDatabase,
  FirebaseListObservable
} from 'angularfire2/database'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'
import { SplashScreen } from '@ionic-native/splash-screen'
import { BackgroundMode } from '@ionic-native/background-mode'
import { Storage } from '@ionic/storage'

import { MusicService } from '../../providers/musicservice'
import { Player } from '../player/player'
import { Artist } from '../artist/artist'
import { Menu } from '../menu/menu'
import { Album } from '../album/album'
import { Video } from '../video/video'
// import { AuthService } from '../../providers/auth-service'
import { MusicData } from '../../providers/music-data'
import { WelcomePage } from '../welcome/welcome'
import { RegionPage } from '../region/region'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  tracks: any
  newVideos: any[] = []
  newArtists: FirebaseListObservable<any>
  newAlbums: FirebaseListObservable<any>
  newTracks: FirebaseListObservable<any>
  allTracks: FirebaseListObservable<any>
  allVideos: FirebaseListObservable<any>
  allAlbums: FirebaseListObservable<any>
  allArtists: FirebaseListObservable<any>
  popularTracks: FirebaseListObservable<any>
  popularVideos: FirebaseListObservable<any>
  popularAlbums: FirebaseListObservable<any>
  popularArtists: FirebaseListObservable<any>
  newPlaylists: FirebaseListObservable<any>
  regions: FirebaseListObservable<any>
  myTracks: any
  playlists: any[] = []
  regionPlaylists: any[] = [] //Region playlist Array holder
  videoPlaylists: any[] = []
  popularTracksPlaylists: any[] = []
  popularVideosPlaylists: any[] = []
  popularArtistsPlaylists: any[] = []
  popularAlbumsPlaylists: any[] = []
  regionsList: any[] = []
  videob: boolean = false
  trackb: boolean = false
  artistb: boolean = false
  albumb: boolean = false
  loadedNewAlbum: boolean = false
  constructor(
    public storage: Storage,
    public bg: BackgroundMode,
    public splashScreen: SplashScreen,
    public alert: AlertController,
    public platform: Platform,
    public md: MusicData,
    // private _auth: AuthService,
    public navCtrl: NavController,
    public musicservice: MusicService,
    public af: AngularFireDatabase,
    public navParam: NavParams
  ) {
    storage.get('follow').then(val => {
      if (val !== null) {
      } else {
        this.navCtrl.push(WelcomePage)
        storage.set('follow', true)
      }
    })

    platform.ready().then(() => {
      if (this.splashScreen) {
        setTimeout(() => {
          this.splashScreen.hide()
        }, 500)
      }

      platform.registerBackButtonAction(() => {
        if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop()
        } else {
          this.bg.setDefaults({
            title: 'Zitagon Music',
            text: 'Playing',
            icon: 'icon'
          })

          this.bg.moveToBackground()
        }
      })
    })

    this.allAlbums = this.md.albums
    this.allArtists = this.md.artists
    this.allTracks = this.md.tracks
    this.allVideos = this.md.videos
    this.regions = this.md.regions

    this.md.newAlbums.subscribe(x => {
      this.loadedNewAlbum = true
    })
    this.newArtists = this.md.newArtists
    this.newAlbums = this.md.newAlbums
    this.newTracks = this.md.newTracks
    this.newPlaylists = this.md.newPlaylists
    this.popularTracks = this.md.popularTrack
    this.popularVideos = this.md.popularVideos
    this.popularAlbums = this.md.popularAlbum
    this.popularArtists = this.md.popularArtist

    this.newTracks.subscribe(snapshots => {
      this.playlists = []
      snapshots.forEach(element => {
        this.playlists.push({
          src: element.url,
          video: element.video,
          artist: element.artistName,
          title: element.name,
          art: element.albumArt,
          preload: 'metadata',
          key: element.$key,
          artistId: element.artist,
          albumId: element.album,
          album: element.albumName,
          artistName: element.artistName,
          collaborations: element.collaborations,
          featuring: element.featuring
        })
      })
    })

    this.allVideos.subscribe(snapshots => {
      this.videoPlaylists = []
      snapshots.forEach(element => {
        if (element.video) {
          this.videoPlaylists.push({
            src: element.url,
            video: element.video,
            artist: element.artistName,
            title: element.name,
            art: element.albumArt,
            preload: 'metadata',
            key: element.$key,
            artistId: element.artist,
            albumId: element.album,
            album: element.albumName,
            artistName: element.artistName,
            collaborations: element.collaborations,
            featuring: element.featuring
          })
        }
      })
    })

    this.popularVideos.subscribe(pop => {
      if (this.videob == false) {
        this.popularVideosPlaylists = []
        pop.forEach(pope => {
          this.allTracks.subscribe(all => {
            all.forEach(alle => {
              if (alle.$key == pope.$key) {
                this.popularVideosPlaylists.push({
                  src: alle.url,
                  video: alle.video,
                  artist: alle.artistName,
                  title: alle.name,
                  art: alle.albumArt,
                  preload: 'metadata',
                  key: alle.$key,
                  artistId: alle.artist,
                  albumId: alle.album,
                  album: alle.albumName,
                  artistName: alle.artistName,
                  collaborations: alle.collaborations,
                  featuring: alle.featuring
                })
              }
            })
          })
        })
        this.videob = true
      }
    })

    this.popularTracks.subscribe(pop => {
      if (this.trackb == false) {
        this.popularTracksPlaylists = []
        pop.forEach(pope => {
          this.allTracks.subscribe(all => {
            all.forEach(alle => {
              if (alle.$key == pope.$key) {
                this.popularTracksPlaylists.push({
                  src: alle.url,
                  video: alle.video,
                  artist: alle.artistName,
                  title: alle.name,
                  art: alle.albumArt,
                  preload: 'metadata',
                  key: alle.$key,
                  artistId: alle.artist,
                  albumId: alle.album,
                  album: alle.albumName,
                  artistName: alle.artistName,
                  collaborations: alle.collaborations,
                  featuring: alle.featuring
                })
              }
            })
          })
        })
        this.trackb = true
      }
    })

    this.popularArtists.subscribe(pop => {
      if (this.artistb == false) {
        this.popularArtistsPlaylists = []
        pop.forEach(pope => {
          this.allArtists.subscribe(all => {
            all.forEach(alle => {
              if (alle.$key == pope.$key) {
                this.popularArtistsPlaylists.push(alle)
              }
            })
          })
        })
        this.artistb = true
      }
    })

    this.tracks = this.musicservice.audio.tracks[0]
  }

  gotoartist(artistKey) {
    this.navCtrl.push(Artist, { artistKey: artistKey })
  }

  gotoalbum(albumKey, artistKey) {
    this.navCtrl.push(Album, {
      albumKey: albumKey,
      artistKey: artistKey
    })
  }

  menu() {
    this.navCtrl.push(Menu)
  }

  ngAfterContentInit() {
    // get all tracks managed by AudioProvider so we can control playback via the APIs
    this.tracks = this.musicservice.audio.tracks[0]

    IntervalObservable.create(200).subscribe(n => {
      this.tracks = this.musicservice.audio.tracks[0]
    })
  }

  ionViewWillEnter() {
    this.tracks = this.musicservice.audio.tracks[0]
  }

  playVideo(track) {
    this.musicservice.pause()
    this.navCtrl.push(Video, {
      videos: this.videoPlaylists,
      video: this.videoPlaylists[track]
    })
  }

  gotoRegion(regionkey) {
    this.navCtrl.push(RegionPage, {
      region: regionkey
    })
    //pass params
  }

  playpopVideo(track) {
    this.musicservice.pause()
    this.navCtrl.push(Video, {
      videos: this.popularVideosPlaylists,
      video: this.popularVideosPlaylists[track]
    })
  }

  play(track) {
    this.musicservice.play(this.playlists, track)
    this.tracks = this.musicservice.audio.tracks[0]
  }

  playpop(track) {
    this.musicservice.play(this.popularTracksPlaylists, track)
    this.tracks = this.musicservice.audio.tracks[0]
  }

  next() {
    this.musicservice.next()
    this.tracks = this.musicservice.audio.tracks[0]
  }

  finish() {
    this.musicservice.finish()
  }

  player() {
    this.navCtrl.push(Player)
  }
}
