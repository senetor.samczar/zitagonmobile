import { Component } from '@angular/core'
import { NavController, NavParams, Platform } from 'ionic-angular'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'
import { BackgroundMode } from '@ionic-native/background-mode'

// import { AuthService } from '../../providers/auth-service'
import { MusicService } from '../../providers/musicservice'
import { Favorite } from '../../providers/favorite'
import { RegionPage } from '../region/region'

@Component({
  selector: 'page-explore',
  templateUrl: 'explore.html'
})
export class ExplorePage {
  tabBarElement: any
  constructor(
    public bg: BackgroundMode,
    public platform: Platform,
    public favorite: Favorite,
    // public actionSheetCtrl: ActionSheetController,
    public params: NavParams,
    // private _auth: AuthService,
    public navCtrl: NavController,
    public musicservice: MusicService
  ) {
    platform.ready().then(() => {
      platform.registerBackButtonAction(() => {
        if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop()
        } else {
          this.bg.moveToBackground()
        }
      })
    })
  }
  region() {
    this.navCtrl.push(RegionPage)
  }
}
