import { Component } from '@angular/core'
import { NavController, NavParams } from 'ionic-angular'

@Component({
  selector: 'page-followed-artists',
  templateUrl: 'followed-artists.html'
})
export class FollowedArtistsPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {}
}
