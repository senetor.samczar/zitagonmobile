import { Component } from '@angular/core'
import { NavController, NavParams, Platform } from 'ionic-angular'
import {
  AngularFireDatabase,
  FirebaseListObservable
} from 'angularfire2/database'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'
import { BackgroundMode } from '@ionic-native/background-mode'

import { MusicService } from '../../providers/musicservice'
import { Favorite } from '../../providers/favorite'
import { MusicData } from '../../providers/music-data'
import { IntervalObservable } from 'rxjs/observable/IntervalObservable'
import { Player } from '../player/player'
import { Artist } from '../artist/artist'
import { Album } from '../album/album'
import { Video } from '../video/video'
// import { AuthService } from '../../providers/auth-service'

@Component({
  selector: 'page-country',
  templateUrl: 'country.html'
})
export class CountryPage {
  tabBarElement: any
  country: any
  tracks: any
  newVideos: any[] = []
  newArtists: FirebaseListObservable<any>
  newAlbums: FirebaseListObservable<any>
  newTracks: FirebaseListObservable<any>
  allTracks: FirebaseListObservable<any>
  allVideos: FirebaseListObservable<any>
  allAlbums: FirebaseListObservable<any>
  allArtists: FirebaseListObservable<any>
  popularTracks: FirebaseListObservable<any>
  popularVideos: FirebaseListObservable<any>
  popularAlbums: FirebaseListObservable<any>
  popularArtists: FirebaseListObservable<any>
  newPlaylists: FirebaseListObservable<any>
  countryRegion: FirebaseListObservable<any>
  artistCountry: FirebaseListObservable<any>
  myTracks: any
  playlists: any[] = []
  regionPlaylists: any[] = [] //Region playlist Array holder
  videoPlaylists: any[] = []
  popularTracksPlaylists: any[] = []
  popularVideosPlaylists: any[] = []
  popularArtistsPlaylists: any[] = []
  popularAlbumsPlaylists: any[] = []
  regionsList: any[] = []
  curcountryList = []
  videob: boolean = false
  trackb: boolean = false
  artistb: boolean = false
  albumb: boolean = false
  loadedNewAlbum: boolean = false
  countryList = []
  artistList = []
  newlists: any[] = []

  constructor(
    public bg: BackgroundMode,
    public platform: Platform,
    public favorite: Favorite,
    // public actionSheetCtrl: ActionSheetController,
    public params: NavParams,
    // private _auth: AuthService,
    public navCtrl: NavController,
    public musicservice: MusicService,
    public md: MusicData,
    public af: AngularFireDatabase
  ) {
    platform.ready().then(() => {
      platform.registerBackButtonAction(() => {
        if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop()
        } else {
          this.bg.moveToBackground()
        }
      })
    })
  }

  ionViewDidLoad() {
    this.country = this.params.get('country')
    this.allAlbums = this.md.albums
    this.allArtists = this.md.artists
    this.allTracks = this.md.tracks
    this.allVideos = this.md.videos

    this.md.newAlbums.subscribe(x => {
      this.loadedNewAlbum = true
    })
    this.newArtists = this.md.newArtists
    this.newAlbums = this.md.newAlbums
    this.newTracks = this.md.newTracks
    this.newPlaylists = this.md.newPlaylists
    this.popularTracks = this.md.popularTrack
    this.popularVideos = this.md.popularVideos
    this.popularAlbums = this.md.popularAlbum
    this.popularArtists = this.md.popularArtist

    this.allArtists.subscribe(all => {
      this.artistList = []
      all.forEach(alle => {
        if (this.country.name == alle.countryName) this.artistList.push(alle)
      })

      this.loadData()
    })
  }

  loadData() {
    this.newTracks.subscribe(snapshots => {
      snapshots.forEach(element => {
        if (this.checkArtist(element.artist)) {
          this.newlists.push({
            src: element.url,
            video: element.video,
            artist: element.artistName,
            title: element.name,
            art: element.albumArt,
            preload: 'metadata',
            key: element.key,
            artistId: element.artist,
            albumId: element.album,
            album: element.albumName,
            collaborations: element.collaborations,
            featuring: element.featuring
          })
        }
      })
    })

    this.allVideos.subscribe(snapshots => {
      this.videoPlaylists = []
      snapshots.forEach(element => {
        if (element.video && this.checkArtist(element.artist)) {
          this.videoPlaylists.push({
            src: element.url,
            video: element.video,
            artist: element.artistName,
            title: element.name,
            art: element.albumArt,
            preload: 'metadata',
            key: element.$key,
            artistId: element.artist,
            albumId: element.album,
            album: element.albumName,
            artistName: element.artistName,
            collaborations: element.collaborations,
            featuring: element.featuring
          })
        }
      })
    })

    this.popularTracks.subscribe(pop => {
      if (this.trackb == false) {
        this.popularTracksPlaylists = []
        pop.forEach(pope => {
          this.allTracks.subscribe(all => {
            all.forEach(alle => {
              if (alle.$key == pope.$key && this.checkArtist(alle.artist)) {
                this.popularTracksPlaylists.push({
                  src: alle.url,
                  video: alle.video,
                  artist: alle.artistName,
                  title: alle.name,
                  art: alle.albumArt,
                  preload: 'metadata',
                  key: alle.$key,
                  artistId: alle.artist,
                  albumId: alle.album,
                  album: alle.albumName,
                  artistName: alle.artistName,
                  collaborations: alle.collaborations,
                  featuring: alle.featuring
                })
              }
            })
          })
        })
        this.trackb = true
      }
    })

    this.popularArtists.subscribe(pop => {
      if (this.artistb == false) {
        this.popularArtistsPlaylists = []
        pop.forEach(pope => {
          this.allArtists.subscribe(all => {
            all.forEach(alle => {
              if (alle.$key == pope.$key && this.checkArtist(alle.$key)) {
                this.popularArtistsPlaylists.push(alle)
              }
            })
          })
        })
        this.artistb = true
      }
    })

    this.af.list('/tracks/').subscribe(snapshots => {
      snapshots.forEach(element => {
        if (this.checkArtist(element.artist)) {
          this.playlists.push({
            src: element.url,
            video: element.video,
            artist: element.artistName,
            title: element.name,
            art: element.albumArt,
            preload: 'metadata',
            key: element.key,
            artistId: element.artist,
            albumId: element.album,
            album: element.albumName,
            collaborations: element.collaborations,
            featuring: element.featuring
          })
        }
      })
    })

    this.tracks = this.musicservice.audio.tracks[0]
  }

  checkArtist(artistId) {
    for (let i = 0; i < this.artistList.length; i++) {
      if (this.artistList[i].$key == artistId) return true
    }
    return false
  }

  gotocountry(country) {
    this.navCtrl.push(CountryPage, { country: country })
  }

  AddUpdateArtistList(item) {
    let bAdd = true
    for (var i = 0; i < this.popularArtistsPlaylists.length; i++) {
      if (item.$key == this.popularArtistsPlaylists[i].$key) {
        bAdd = false
        break
      }
    }
    if (bAdd) this.popularArtistsPlaylists.push(item)
  }

  gotoartist(artistKey) {
    this.navCtrl.push(Artist, { artistKey: artistKey })
  }

  gotoalbum(albumKey, artistKey) {
    this.navCtrl.push(Album, {
      albumKey: albumKey,
      artistKey: artistKey
    })
  }

  ngAfterContentInit() {
    // get all tracks managed by AudioProvider so we can control playback via the APIs
    this.tracks = this.musicservice.audio.tracks[0]

    IntervalObservable.create(200).subscribe(n => {
      this.tracks = this.musicservice.audio.tracks[0]
    })
  }

  ionViewWillEnter() {
    this.tracks = this.musicservice.audio.tracks[0]
  }

  playVideo(track) {
    this.musicservice.pause()
    this.navCtrl.push(Video, {
      videos: this.videoPlaylists,
      video: this.videoPlaylists[track]
    })
  }

  playpopVideo(track) {
    this.musicservice.pause()
    this.navCtrl.push(Video, {
      videos: this.popularVideosPlaylists,
      video: this.popularVideosPlaylists[track]
    })
  }

  play(track) {
    this.musicservice.play(this.playlists, track)
    this.tracks = this.musicservice.audio.tracks[0]
  }

  playnew(track) {
    this.musicservice.play(this.newlists, track)
    this.tracks = this.musicservice.audio.tracks[0]
  }

  playpop(track) {
    this.musicservice.play(this.popularTracksPlaylists, track)
    this.tracks = this.musicservice.audio.tracks[0]
  }

  next() {
    this.musicservice.next()
    this.tracks = this.musicservice.audio.tracks[0]
  }

  finish() {
    this.musicservice.finish()
  }

  player() {
    this.navCtrl.push(Player)
  }
  back() {
    this.navCtrl.pop()
  }
}
