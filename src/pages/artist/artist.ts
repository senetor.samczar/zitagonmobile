import { Component } from '@angular/core'
import {
  NavController,
  NavParams,
  Platform,
  ActionSheetController
} from 'ionic-angular'
import { IntervalObservable } from 'rxjs/observable/IntervalObservable'
import {
  AngularFireDatabase,
  FirebaseListObservable
} from 'angularfire2/database'
import { BackgroundMode } from '@ionic-native/background-mode'

import { MusicService } from '../../providers/musicservice'
import { Player } from '../player/player'
import { Album } from '../album/album'
// import { AuthService } from '../../providers/auth-service'
import { Favorite } from '../../providers/favorite'
import { Video } from '../video/video'
import { Share } from '../share/share'

@Component({
  selector: 'page-artist',
  templateUrl: 'artist.html'
})
export class Artist {
  actions: any
  tracks: any
  trackss: any
  albums: FirebaseListObservable<any>
  artistImage: any
  artistKey: number
  artistName: any
  playlists: any[] = []
  videolists: any[] = []
  playlists_latest: any[] = []
  tabBarElement: any
  follow: any
  followNum: number = 0
  opened: boolean = false

  constructor(
    public fav: Favorite,
    public bg: BackgroundMode,
    public platform: Platform,
    public params: NavParams,
    //private _auth: AuthService,
    public navCtrl: NavController,
    public musicservice: MusicService,
    public actionSheetCtrl: ActionSheetController,
    public af: AngularFireDatabase
  ) {
    platform.ready().then(() => {
      platform.registerBackButtonAction(() => {
        if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop()
        } else {
          this.bg.moveToBackground()
        }
      })
    })

    this.tabBarElement = document.querySelector('.tabbar.show-tabbar')
    this.tabBarElement.style.display = 'none'
    this.artistKey = this.params.get('artistKey')

    this.albums = af.list('/albums/', {
      query: {
        orderByChild: 'artist',
        equalTo: this.artistKey
      }
    })

    this.af
      .list('/artists', {
        preserveSnapshot: true,
        query: {
          orderByKey: true,
          equalTo: this.artistKey
        }
      })
      .subscribe(snapshots => {
        this.artistName = snapshots[0].val().name
        this.artistImage = snapshots[0].val().cover
      })

    this.trackss = af.list('/tracks/', {
      query: {
        orderByChild: 'artist',
        equalTo: this.artistKey
      }
    })

    this.trackss.subscribe(snapshots => {
      snapshots.forEach(element => {
        this.artistName = element.artistName
        if (element.video) {
          this.videolists.push({
            src: element.url,
            video: element.video,
            artist: element.artistName,
            title: element.name,
            art: element.albumArt,
            preload: 'metadata',
            key: element.$key,
            artistId: element.artist,
            albumId: element.album,
            album: element.albumName,
            artistName: element.artistName,
            collaborations: element.collaborations,
            featuring: element.featuring
          })
        }
        this.playlists.push({
          src: element.url,
          artist: element.artistName,
          title: element.name,
          art: element.albumArt,
          preload: 'metadata',
          key: element.$key,
          artistId: element.artist,
          albumId: element.album,
          album: element.albumName,
          timestamp: element.timestamp
        })
      })
      this.arrangeList()
    })

    this.tracks = this.musicservice.audio.tracks[0]
  }

  back() {
    this.navCtrl.pop()
  }

  more(track) {
    this.actions = this.actionSheetCtrl.create({
      title: track.name,
      buttons: [
        {
          text: 'Add to favorites',
          handler: () => {
            this.opened = false

            this.fav.favorite(track.$key)
          }
        },
        {
          text: 'Add to Playlist',
          handler: () => {
            this.opened = false
            this.fav.playlistAlert(track.$key)
          }
        },
        {
          text: 'Go to Artist',
          handler: () => {
            this.opened = false

            this.gotoartist(track)
          }
        },
        {
          text: 'Share',
          handler: () => {
            this.opened = false

            this.shareTrack(track)
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.opened = false
          }
        }
      ]
    })

    this.actions.present()
    this.opened = true
  }

  gotoartist(track) {
    this.navCtrl.push(Artist, { artistKey: track.artist })
  }

  shareTrack(track) {
    this.navCtrl.push(Share, {
      title: track.name,
      artist: track.artistName,
      art: track.albumArt,
      album: track.albumName
    })
  }

  arrangeList() {
    this.playlists_latest = []
    for (let i = 0; i < this.playlists.length; i++) {
      this.playlists_latest.push(this.playlists[i])
    }
    for (let i = 0; i < this.playlists_latest.length - 1; i++) {
      for (let j = i + 1; j < this.playlists_latest.length; j++) {
        if (
          this.playlists_latest[i].timestamp <
          this.playlists_latest[j].timestamp
        ) {
          let temp = this.playlists_latest[i].timestamp
          this.playlists_latest[i].timestamp = this.playlists_latest[
            j
          ].timestamp
          this.playlists_latest[j].timestamp = temp
        }
      }
    }
  }

  playVideo(track) {
    this.musicservice.pause()
    this.navCtrl.push(Video, {
      videos: this.videolists,
      video: this.videolists[track]
    })
  }

  play(track) {
    this.musicservice.play(this.playlists, track)
    this.tracks = this.musicservice.audio.tracks[0]
  }

  shuffle() {
    let t = Math.floor(Math.random() * this.playlists.length + 0)
    this.musicservice.play(this.playlists, t)
    this.tracks = this.musicservice.audio.tracks[0]
    this.musicservice.shuffle = true
  }

  gotoalbum(albumKey, artistKey) {
    this.navCtrl.push(Album, {
      albumKey: albumKey,
      artistKey: artistKey
    })
  }

  ngAfterContentInit() {
    // get all tracks managed by AudioProvider so we can control playback via the APIs
    this.tracks = this.musicservice.audio.tracks[0]

    IntervalObservable.create(200).subscribe(n => {
      this.tracks = this.musicservice.audio.tracks[0]
    })
  }
  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex'
  }

  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none'

    this.tracks = this.musicservice.audio.tracks[0]

    this.follow = this.af.list('followedArtist/' + this.artistKey)

    this.follow.subscribe(like => {
      if (like[0] != undefined) this.followNum = like[0].$value
      else this.followNum = 0
    })
  }

  next() {
    this.musicservice.next()
    this.tracks = this.musicservice.audio.tracks[0]
  }

  finish() {
    this.musicservice.finish()
  }

  player() {
    this.navCtrl.push(Player)
  }
}
