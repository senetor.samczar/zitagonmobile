import { Component } from '@angular/core'
import { Platform, ToastController } from 'ionic-angular'
import { SplashScreen } from '@ionic-native/splash-screen'
import { Network } from '@ionic-native/network'

import { LibraryPage } from '../library/library'
import { SearchPage } from '../search/search'
import { HomePage } from '../home/home'
import { ExplorePage } from '../explore/explore'

export enum ConnectionStatusEnum {
  Online,
  Offline
}

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root = HomePage
  tab2Root = SearchPage
  tab3Root = LibraryPage
  tab4Root = ExplorePage
  previousStatus
  toast

  constructor(
    platform: Platform,
    splashScreen: SplashScreen,
    private network: Network,
    private toastCtrl: ToastController
  ) {}

  ionViewDidLoad() {
    this.network.onDisconnect().subscribe(() => {
      if (
        !this.previousStatus ||
        this.previousStatus === ConnectionStatusEnum.Online
      ) {
        this.toast = this.toastCtrl.create({
          message:
            'Failed to connect to server. Please check your internet connection and try again.'
        })
        this.toast.present()
      }
      this.previousStatus = ConnectionStatusEnum.Offline
    })
    this.network.onConnect().subscribe(() => {
      if (
        !this.previousStatus ||
        this.previousStatus === ConnectionStatusEnum.Offline
      ) {
        if (this.toast) this.toast.dismiss()
      }
      this.previousStatus = ConnectionStatusEnum.Online
    })
  }
}
