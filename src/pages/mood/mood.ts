import { Component } from '@angular/core'
import {
  NavController,
  NavParams,
  ActionSheetController,
  Platform
} from 'ionic-angular'
import { IntervalObservable } from 'rxjs/observable/IntervalObservable'
import { BackgroundMode } from '@ionic-native/background-mode'
import {
  AngularFireDatabase,
  FirebaseListObservable
} from 'angularfire2/database'

import { Player } from '../player/player'
import { MusicService } from '../../providers/musicservice'
// import { AuthService } from '../../providers/auth-service'
import { Favorite } from '../../providers/favorite'
import { Artist } from '../artist/artist'
import { Album } from '../album/album'
import { Share } from '../share/share'
import { MusicData } from '../../providers/music-data'

@Component({
  selector: 'page-mood',
  templateUrl: 'mood.html'
})
export class Mood {
  tracks: any
  playlists: any[] = []
  trackss: FirebaseListObservable<any>
  mood: any
  tabBarElement: any
  actions: any
  opened: boolean = false
  newTracks: FirebaseListObservable<any>
  popularTracks: FirebaseListObservable<any>
  allTracks: FirebaseListObservable<any>
  popularArtists: FirebaseListObservable<any>
  allArtists: FirebaseListObservable<any>
  popularTracksPlaylists: any[] = []
  popularArtistsPlaylists: any[] = []
  newlists = []
  trackb: boolean = false
  artistb = false
  constructor(
    public bg: BackgroundMode,
    public platform: Platform,
    public favorite: Favorite,
    public actionSheetCtrl: ActionSheetController,
    public params: NavParams,
    // private _auth: AuthService,
    public navCtrl: NavController,
    public musicservice: MusicService,
    public md: MusicData,
    public af: AngularFireDatabase
  ) {
    platform.ready().then(() => {
      platform.registerBackButtonAction(() => {
        if (this.opened) {
          this.opened = false
          setTimeout(() => {
            this.actions.dismiss()
          }, 100)
        } else if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop()
        } else {
          this.bg.moveToBackground()
        }
      })
    })

    this.tabBarElement = document.querySelector('.tabbar.show-tabbar')
    this.tabBarElement.style.display = 'none'
    this.mood = this.params.get('mood')
    this.allTracks = this.md.tracks
    this.newTracks = this.md.newTracks
    this.popularTracks = this.md.popularTrack
    this.allArtists = this.md.artists
    this.popularArtists = this.md.popularArtist

    this.newTracks.subscribe(snapshots => {
      this.newlists = []
      snapshots.forEach(element => {
        let b = false
        for (var i = 0; i < element.mood.length; i++) {
          if (element.mood[i].name == this.mood.val().name) {
            b = true
            break
          }
        }
        if (b) {
          this.newlists.push({
            src: element.url,
            video: element.video,
            artist: element.artistName,
            title: element.name,
            art: element.albumArt,
            preload: 'metadata',
            key: element.$key,
            artistId: element.artist,
            albumId: element.album,
            album: element.albumName,
            artistName: element.artistName,
            collaborations: element.collaborations,
            featuring: element.featuring
          })
        }
      })
    })
    this.popularTracks.subscribe(pop => {
      if (this.trackb == false) {
        this.popularTracksPlaylists = []
        pop.forEach(pope => {
          this.allTracks.subscribe(all => {
            all.forEach(alle => {
              let b = false
              for (var i = 0; i < alle.mood.length; i++) {
                if (alle.mood[i].name == this.mood.val().name) {
                  b = true
                  break
                }
              }
              if (alle.$key == pope.$key && b) {
                this.popularTracksPlaylists.push({
                  src: alle.url,
                  video: alle.video,
                  artist: alle.artistName,
                  title: alle.name,
                  art: alle.albumArt,
                  preload: 'metadata',
                  key: alle.$key,
                  artistId: alle.artist,
                  albumId: alle.album,
                  album: alle.albumName,
                  artistName: alle.artistName,
                  collaborations: alle.collaborations,
                  featuring: alle.featuring
                })
              }
            })
          })
        })
        this.trackb = true
      }
    })

    af.list('/tracks/').subscribe(snapshots => {
      snapshots.forEach(element => {
        for (var i = 0; i < element.mood.length; i++) {
          if (element.mood[i].name == this.mood.val().name) {
            this.allArtists.subscribe(all => {
              all.forEach(alle => {
                if (alle.$key == element.artist) {
                  this.AddUpdateArtistList(alle)
                }
              })
            })
            this.playlists.push({
              src: element.url,
              video: element.video,
              artist: element.artistName,
              title: element.name,
              art: element.albumArt,
              preload: 'metadata',
              key: element.key,
              artistId: element.artist,
              albumId: element.album,
              album: element.albumName,
              collaborations: element.collaborations,
              featuring: element.featuring
            })
          }
        }
      })
    })

    this.tracks = this.musicservice.audio.tracks[0]
  }

  AddUpdateArtistList(item) {
    let bAdd = true
    for (var i = 0; i < this.popularArtistsPlaylists.length; i++) {
      if (item.$key == this.popularArtistsPlaylists[i].$key) {
        bAdd = false
        break
      }
    }
    if (bAdd) this.popularArtistsPlaylists.push(item)
  }

  ngAfterContentInit() {
    // get all tracks managed by AudioProvider so we can control playback via the APIs
    this.tracks = this.musicservice.audio.tracks[0]

    IntervalObservable.create(200).subscribe(n => {
      this.tracks = this.musicservice.audio.tracks[0]
    })
  }

  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex'
  }

  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none'

    this.tracks = this.musicservice.audio.tracks[0]
  }

  play(track) {
    this.musicservice.play(this.playlists, track)
    this.tracks = this.musicservice.audio.tracks[0]
  }

  playnew(track) {
    this.musicservice.play(this.newlists, track)
    this.tracks = this.musicservice.audio.tracks[0]
  }

  shuffle() {
    let t = Math.floor(Math.random() * this.playlists.length + 0)
    this.musicservice.play(this.playlists, t)
    this.tracks = this.musicservice.audio.tracks[0]
    this.musicservice.shuffle = true
  }

  next() {
    this.musicservice.next()
    this.tracks = this.musicservice.audio.tracks[0]
  }

  finish() {
    this.musicservice.finish()
  }

  player() {
    this.navCtrl.push(Player)
  }

  back() {
    this.navCtrl.pop()
  }

  playpop(track) {
    this.musicservice.play(this.popularTracksPlaylists, track)
    this.tracks = this.musicservice.audio.tracks[0]
  }

  gotoartist(artistKey) {
    this.navCtrl.push(Artist, { artistKey: artistKey })
  }

  gotoalbum(track) {
    this.navCtrl.push(Album, {
      albumKey: track.albumId,
      artistKey: track.artistId
    })
  }

  more(track) {
    this.actions = this.actionSheetCtrl.create({
      title: track.name,
      buttons: [
        {
          text: 'Add to favorite',
          handler: () => {
            this.opened = false
            this.favorite.favorite(track.key)
          }
        },
        {
          text: 'Add to Playlist',
          handler: () => {
            this.opened = false
            this.favorite.playlistAlert(track.key)
          }
        },
        {
          text: 'Go to Artist',
          handler: () => {
            this.opened = false
            this.gotoartist(track)
          }
        },
        {
          text: 'Go to Album',
          handler: () => {
            this.opened = false
            this.gotoalbum(track)
          }
        },
        {
          text: 'Share',
          handler: () => {
            this.opened = false
            this.shareTrack(track)
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.opened = false
          }
        }
      ]
    })

    this.actions.present()
    this.opened = true
  }

  shareTrack(track) {
    this.navCtrl.push(Share, {
      title: track.name,
      artist: track.artistName,
      art: track.albumArt,
      album: track.albumName
    })
  }
}
